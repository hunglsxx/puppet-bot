#!/usr/bin/env node

import { Command, Option, Argument } from 'commander';
import { CommandUtil as Util } from '../classes/utils/Command';
import * as _ from 'lodash';
import { Reddit } from '../bots/Reddit';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import queue from 'queue';

const util = new Util();
const program = new Command();

program
    .name('Auto Reddit')
    .description('CLI to auto reddit')
    .version('0.0.1');

program.command('auto')
    .description('Auto vote post or comment on reddit with search keyword')
    .addArgument(new Argument('<action>', 'Action you want')
        .choices(['vote', 'comment']))
    .addOption(new Option('-vt, --vote-type <type>', 'Vote type')
        .default('up')
        .choices(['up', 'down']))
    .addOption(new Option('-pt, --post-type <type>', 'Post type')
        .default('post')
        .choices(['post', 'comment', 'both']))
    .option('-ac, --account <string>', 'Your reddit account [username,password]', util.accountParse)
    .option('-kw, --keywords <string...>', 'Your keywords, separated by a space')
    .option('-cm, --comments [string...]', 'Your comment content')
    .addOption(new Option('-ss, --search-sort <string>', 'Reddit search result sorting')
        .default('hot')
        .choices(['hot', 'top', 'new', 'relevance', 'comments']))
    .option('-fac, --file-account <string>', 'CSV file path contain your reddit accounts (2 column: username, password), column separated by a "," charactor', util.fileParse)
    .option('-fkw, --file-keyword <string>', 'CSV file path contain your keywords (1 column: keyword)', util.fileParse)
    .option('-fcm, --file-comment <string>', 'CSV file path contain your comment contents (1 column: comment)', util.fileParse)
    .action(async (action, options) => {

        var accounts = _.concat(options.account, options.fileAccount);
        accounts = _.compact(accounts);
        accounts = _.uniqBy(accounts, 'username');

        var keywords = options.keywords ? [...options.keywords] : [];
        for (let i in options.fileKeyword) {
            keywords.push(options.fileKeyword[i].keyword);
        }
        keywords = _.compact(keywords);
        keywords = _.uniq(keywords);

        if (accounts && accounts.length && keywords && keywords.length) {
            for (let a in accounts) {
                let loginReddit = new Reddit();
                let browser = loginReddit.browser;
                let qSearch = queue({ results: [], concurrency: 5, autostart: false });

                await loginReddit.init(loginReddit.getUri('login'), [StealthPlugin()]);

                if (await loginReddit.validate()) {
                    await loginReddit.open();
                    await loginReddit.signIn(accounts[a].username, accounts[a].password);

                    for (let k in keywords) {
                        qSearch.push(async function (cb: any) {
                            let searchReddit = new Reddit(browser);
                            await searchReddit.init(`${loginReddit.getUri('search')}/?q=${keywords[k]}&sort=${options.searchSort}`, [StealthPlugin()]);
                            await searchReddit.open();
                            await searchReddit.scroll();
                            let links = await searchReddit.getPostUrls();
                            await searchReddit.close();
                            cb(null, links);
                        });
                    }
                    qSearch.start(async function (err: any) {
                        if (err) console.log(err);

                        for (let k in qSearch.results) {
                            let rsk = qSearch.results[k];
                            for (let m in rsk) {
                                let rsm = rsk[m];
                                for (let l in rsm) {
                                    let link = rsm[l];
                                    qSearch.push(async function (cbl: any) {
                                        let actionReddit = new Reddit(browser);
                                        await actionReddit.init(`${link}`, [StealthPlugin()]);
                                        await actionReddit.open();
                                        switch (action) {
                                            case 'vote':
                                                await actionReddit.vote(options.voteType, options.postType);
                                                break;
                                            case 'comment':
                                                var comments = options.comments ? [...options.comments] : [];
                                                for (let i in options.fileComment) {
                                                    comments.push(options.fileComment[i].comment);
                                                }
                                                comments = _.compact(comments);
                                                comments = _.uniq(comments);

                                                if (!comments.length) throw new Error("Không có nội dung comment");
                                                const random = Math.floor(Math.random() * comments.length);
                                                console.log(random, comments[random]);
                                                await actionReddit.comment(comments[random]);
                                                break;
                                            default:
                                                break;
                                        }
                                        await actionReddit.close();
                                        cbl(null, true);
                                    });
                                }
                            }
                        }
                        qSearch.concurrency = 10;
                        qSearch.start(async function (error: any) {
                            if (error) console.log(error);
                            await browser.close();
                        });
                    });
                }
            }
        }
    });

program.parse();