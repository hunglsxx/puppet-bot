import { Page } from "../classes/Page";

export class Reddit extends Page {
    domain: string = 'reddit.com';
    method: string = 'https';
    validate(): boolean {
        try {
            var domain = this.extractDomain();
            if (domain.includes(this.domain)) {
                return true;
            }
            return false;
        } catch (error) {
            return false;
        }
    }
    getUri(page: string): string {
        switch (page) {
            case 'login':
                return `${this.method}://${this.domain}/login`;
            case 'register':
                return `${this.method}://${this.domain}/register`;
            case 'search':
                return `${this.method}://${this.domain}/search`;
            default:
                break;
        }
        return '';
    }

    signUp(): boolean {
        return false;
    }

    async signIn(username: string, password: string): Promise<boolean> {
        try {
            await this.page.waitForSelector('#loginUsername');
            await this.page.click('#loginUsername');
            await this.page.type('#loginUsername', username);

            await this.page.waitForSelector('#loginPassword');
            await this.page.click('#loginPassword');
            await this.page.type('#loginPassword', password);

            await this.page.waitForSelector('.Step > .Step__content > .AnimatedForm > .AnimatedForm__field > .AnimatedForm__submitButton')
            await this.page.click('.Step > .Step__content > .AnimatedForm > .AnimatedForm__field > .AnimatedForm__submitButton')

            await this.page.waitForNavigation();

            return true;
        } catch (error) {
            return false;
        }
    }

    async comment(content: string): Promise<void> {
        try {
            await this.page.waitForSelector('.DraftEditor-root > .DraftEditor-editorContainer > .notranslate')
            await this.page.click('.DraftEditor-root > .DraftEditor-editorContainer > .notranslate');
            await this.page.type('.DraftEditor-root > .DraftEditor-editorContainer > .notranslate', content);
            const commentBtns = await this.page.$x("//button[contains(text(), 'Comment')]");
            if (commentBtns.length) await commentBtns[0].click();
        } catch (error) {
            console.error(error.message);
        }
    }

    async vote(voteType: string = 'up', postType: string = 'post'): Promise<void> {
        try {
            var selector: string = '//button[contains(@data-click-id, "upvote")]';
            switch (voteType) {
                case 'up':
                    selector = '//button[contains(@data-click-id, "upvote")]';
                    break;
                case 'down':
                    selector = '//button[contains(@data-click-id, "downvote")]';
                    break;
                default:
                    break;
            }
            if (selector != '') {
                const voteBtns = await this.page.$x(selector);
                if (voteBtns.length) {
                    for (let i = 0; i < voteBtns.length; i++) {
                        if (postType == 'post' && i > 0) break;
                        if (postType == 'comment' && i == 0) continue;
                        var pressed = await this.page.evaluate((press: any): void => press.getAttribute("aria-pressed"), voteBtns[i]);
                        if (!pressed) pressed = 'false';
                        else pressed = pressed.trim();
                        if (pressed !== 'true') {
                            await voteBtns[i].click();
                        }
                    }

                }
            }
        } catch (error) {

        }
    }

    async getPostUrls(): Promise<string[]> {
        var selector: string = '//a[contains(@data-click-id, "body")]';
        var postElements = await this.page.$x(selector);
        var data: string[] = [];
        if (postElements.length) {
            for (let i in postElements) {
                var url = await this.page.evaluate((post: any): void => post.getAttribute("href"), postElements[i]);
                if (url) url = `${this.method}://${this.domain}${url}`;
                data.push(url);
            }
        }
        return data;
    }


}