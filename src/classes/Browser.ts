import { executablePath } from 'puppeteer';
import puppeteer from 'puppeteer-extra';
import { Config } from './utils/Config';

export class Browser {
    options: any;
    browser: any;
    config: Config;
    constructor(opts: Object) {
        this.config = new Config();
        const headless = (this.config.get("CHROME_HEADLESS") === 'false') ? false : true;
        var defaultOpts = {
            headless: headless,
            args: [
                '--enable-features=NetworkService',
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-dev-shm-usage',
                '--disable-web-security',
                '--disable-features=IsolateOrigins,site-per-process',
                '--shm-size=3gb',
                '--window-size=1920,1080',
            ],
            ignoreHTTPSErrors: true,
            executablePath: executablePath()
        }
        this.options = { ...defaultOpts, ...opts };
    }

    async init(plugins: any) {
        try {
            if (plugins) {
                for (let i in plugins) {
                    puppeteer.use(plugins[i]);
                }
            }
            this.browser = await puppeteer.launch(this.options);
            return this.browser;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    async close() {
        try {
            await this.browser.close();
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}