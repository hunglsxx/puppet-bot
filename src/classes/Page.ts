import { PageInterface } from "./interfaces/Page";
import { Browser } from "./Browser";

export class Page implements PageInterface {
    href: string;
    requestHeaders: object;
    responseHeaders: object;
    browser: any;
    isSinglePage: boolean;
    page: any;

    constructor(brw: any = false, opts: any = {}) {
        this.isSinglePage = true;
        if (brw) {
            this.browser = brw;
            this.isSinglePage = false;
        } else {
            this.browser = new Browser(opts);
        }
    }

    setResponseHeader(header: object): void {
        this.responseHeaders = header;
    }

    setHref(href: string): void {
        this.href = href;
    }

    async init(href: string, plugins: any): Promise<void> {
        try {
            if (href) this.setHref(href);
            if (this.isSinglePage) await this.browser.init(plugins);

            this.page = await this.browser.browser.newPage();
            await this.page.setViewport({ width: 1920, height: 1080 });
            await this.page.setDefaultNavigationTimeout(0);
            this.page.on('dialog', async (dialog: { accept: () => any; }) => {
                console.log('Dialog confirm');
                await dialog.accept();
            });
        } catch (error) {

        }

    }

    async open(): Promise<boolean> {
        try {
            if (!this.isValidUrl()) return false;
            var response = await this.page.goto(this.href, { waitUntil: 'networkidle2' });
            await this.page.setViewport({ width: 1512, height: 865 });
            console.log("Response status", response.status(), "url:", this.href);
            this.setResponseHeader(response.headers());
            if (response.status() != 200) {
                await this.page.reload();
            }
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    async close(): Promise<boolean> {
        try {
            await this.page.close();
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    getResponseHeader(key: string): string {
        throw new Error("Method not implemented.");
    }

    getRequestHeader(key: string): string {
        throw new Error("Method not implemented.");
    }

    isValidUrl(): boolean {
        try {
            var loc = new URL(this.href);
            if (loc) return true;
            return false;
        } catch (error) {
            return false;
        }
    }

    extractDomain(): string {
        if (this.href && this.isValidUrl()) {
            var loc = new URL(this.href);
            var domain = loc.hostname.replace('www.', '');
            return domain;
        }
        return '';
    }

    async scroll(): Promise<void> {
        await this.page.evaluate(async () => {
            await new Promise((resolve: any) => {
                var totalHeight = 0;
                var distance = 1000;
                var timer = setInterval(() => {
                    var scrollHeight = document.body.scrollHeight;
                    window.scrollBy(0, distance);
                    totalHeight += distance;

                    if (totalHeight >= (scrollHeight - window.innerHeight) || totalHeight >= 3000) {
                        console.log("Stop scroll...");
                        clearInterval(timer);
                        resolve();
                    }
                    console.log("Srolling down...");

                }, 2000);
            });
        });
    }

}