import * as path from 'path';
import * as dotenv from 'dotenv';

export class Config {
    constructor(envPath:string='') { 
        if(envPath == '') {
            envPath = `${path.resolve(path.join(__dirname, '../../'))}/.env`;
        }
        dotenv.config({ path: `${envPath}` });
    }

    get(key: string) {
        return process.env[key] || '';
    }
}
