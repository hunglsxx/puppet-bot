import * as _ from 'lodash';
import { parse } from 'csv-parse/sync';
import { readFileSync } from 'node:fs';

export class CommandUtil {
    accountParse(value: any): object[] {
        if (value) {
            var valueArray = _.split(value, ",");
            if (valueArray.length > 1) {
                return [{
                    'username': valueArray[0],
                    'password': valueArray[1]
                }];
            }
        }
        return [];
    }

    fileParse(value: any): any[] {
        var raw = readFileSync(value);
        const records = parse(raw, {
            columns: true,
            skip_empty_lines: true
        });
        return records;
    }
}