export interface PageInterface {
    href: string;
    requestHeaders: object;
    responseHeaders: object;
    open(): void;
    close(): void;
    setResponseHeader(header: object): void;
    getResponseHeader(key: string): string;
    getRequestHeader(key: string): string;
    setHref(href: string): void;
}